(function (window, Drupal, $) {
  Drupal.behaviors.drafter = {
    attach: function attach(context) {
      // Create a new dialog box, add buttons, and open the dialog.
      confirmationDialog = Drupal.dialog($('#drafter-elements').clone(), {
        resizable: false,
        closeOnEscape: false,
        create: function () {
          // Remove the entire titlebar. If we want to add the titlebar
          // back, note that at minimum we need to remove the native
          // jQuery UI close button (it short-circuits the Drupal.dialog
          // events).
          $(this).find('.ui-dialog-titlebar').remove();
          // Create a Redirect button
          var redirectButton = $('<button/>', {
            text: 'Redirect to draft edit',
            click: function() {
              window.location = drupalSettings.drafter.url;
            }
          });
          // Create a Cancel button to close the dialog.
          var cancelButton = $('<button/>', {
            text: 'Cancel',
            click: function() {
              confirmationDialog.close();
              return false;
            }
          });
          $(this).append(redirectButton);
          $(this).append(cancelButton);
        },
        beforeClose: false,
        close: function (event) {
          Drupal.dialog(event.target).close();
          Drupal.detachBehaviors(event.target, null, 'unload');
          $(event.target).remove();
        },
        afterClose: function () {
          confirmationDialog.open = false;
        }
      });
      confirmationDialog.showModal();
    }
  }
})(window, Drupal, jQuery);
