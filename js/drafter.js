(function (window, Drupal, $) {
  Drupal.behaviors.drafter = {
    attach: function attach(context) {
      // Only do this work when the main form submit button has been loaded.
      var realSubmitButton = $('#edit-submit', context);
      if (!realSubmitButton.length) {
        return;
      }

      // When the node form submit button is clicked, create an
      // interstitial dialog and pop it up instead.
      var confirmationDialog = {};
      realSubmitButton.click(function () {
        var $checkbox = $('#edit-status-value')[0];
        if ($checkbox.checked && (!confirmationDialog.open)) {
          // Create a new dialog box, add buttons, and open the dialog.
          confirmationDialog = Drupal.dialog($('#drafter-elements').clone(), {
            resizable: false,
            closeOnEscape: false,
            create: function () {
              var form = this;
              var $form = $(this);
              var $widget = $form.parent();
              // Remove the entire titlebar. If we want to add the titlebar
              // back, note that at minimum we need to remove the native
              // jQuery UI close button (it short-circuits the Drupal.dialog
              // events).
              $widget.find('.ui-dialog-titlebar').remove();
              // Duplicate the "Save" button and use the duplicate
              // inside the confirmation dialog.
              var isNewDraft = function () {
                return realSubmitButton.data('drafter') === 'newdraft';
              };
              var newDraftButton = realSubmitButton.clone()
                .prop('id', 'edit-submit-drafter')
                .prop('value', isNewDraft() ? 'Create new draft' : 'Take over');

              newDraftButton.click(function () {
                $("input[name='new_draft']").val('yes');
                realSubmitButton.click();
                return false;
              });
              // Create a Cancel button to close the dialog.
              var cancel = $('<button/>', {
                text: 'Cancel',
                click: function() {
                  confirmationDialog.close();
                  return false;
                }
              });
              // Create a "New Revision" button
              var newRevisionButton = $('<button/>', {
                text: 'Save',
                click: function() {
                  realSubmitButton.click();
                }
              });
              $form.append(newDraftButton);
              if (isNewDraft()) {
                $form.append(newRevisionButton);
              }
              $form.append(cancel);
            },
            beforeClose: false,
            close: function (event) {
              Drupal.dialog(event.target).close();
              Drupal.detachBehaviors(event.target, null, 'unload');
              $(event.target).remove();
            },
            afterClose: function () {
              confirmationDialog.open = false;
            }
          });
          confirmationDialog.showModal();
          return false;
        }
      });
    }
  }
})(window, Drupal, jQuery);
