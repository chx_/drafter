<?php

namespace Drupal\drafter\Controller;

use Drupal\Core\Controller\FormController;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\drafter\DrafterHelper;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * An interstitial page for the edit tab.
 *
 * This class extends the parent only to gain access to the protected methods.
 */
class EditInterstitial extends FormController {

  const YES = 'yes';

  /**
   * @var \Drupal\Core\Controller\FormController
   */
  protected $formController;

  public function __construct(FormController $formController) {
    $this->formController = $formController;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    return $this->formController->getFormArgument($route_match);
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    return $this->formController->getFormObject($route_match, $form_arg);
  }

  /**
   * {@inheritdoc}
   */
  public function getContentResult(Request $request, RouteMatchInterface $route_match) {
    $form_arg = $this->getFormArgument($route_match);
    $node = $this->getFormObject($route_match, $form_arg)->getEntity();
    if ($form_arg === 'node.edit' && $node instanceof NodeInterface && self::needsInterstitial($request, $node)) {
      return $this->page($node);
    }
    return $this->formController->getContentResult($request, $route_match);
  }

  public function page(NodeInterface $node) {
    return [[
      '#type' => 'inline_template',
      '#template' => "<p>This {{ type }} already has a draft. Either: </p>
 <ol><li>Edit the {{ draft }} instead.</li>
 <li>Edit the {{ original }}. After saving it, don't forget to edit the draft as well.</li>
 </ol>",
      '#context' => [
        'type' => strtolower($node->type->entity->label()),
        'draft' => Link::fromTextAndUrl(t('draft'), DrafterHelper::getDraft($node)->toUrl('edit-form')),
        'original' => Link::fromTextAndUrl(t('original'), $node->toUrl('edit-form')->setOption('query', ['drafter' => self::YES])),
      ],
    ]];
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\node\NodeInterface $node
   *
   * @return bool
   */
  public static function needsInterstitial(Request $request, NodeInterface $node) {
    return $request->query->get('drafter') !== self::YES && DrafterHelper::getDraft($node);
  }

}
