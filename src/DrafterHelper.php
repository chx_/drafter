<?php

namespace Drupal\drafter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\pathauto\PathautoState;
use Drupal\tmgmt\Entity\JobItem;

class DrafterHelper {

  /**
   * @param \Drupal\node\NodeInterface $node
   */
  public static function addExistingDraftWarning(NodeInterface $node) {
    if (($draftNode = self::getDraft($node)) && !$draftNode->isPublished()) {
      \Drupal::messenger()->addError(t('There is already a <a href="@draft">draft</a>.', [
        '@draft' => $draftNode->toUrl('edit-form')->toString(),
      ]));
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @return bool|\Drupal\node\NodeInterface
   */
  public static function addReplicatedWarning(NodeInterface $node) {
    if (!$node->drafter_parent->isEmpty()) {
      /** @var \Drupal\node\NodeInterface $replicateParent */
      $replicateParent = $node->drafter_parent->entity;
      $args = [
        '@type' => $node->type->entity->label(),
        '@parent' => $replicateParent->toUrl('edit-form')->toString(),
      ];
      if ($node->getChangedTime() < $replicateParent->getChangedTime()) {
        $message = t('This @type is a draft of this <a href="@parent">@type</a> and the latter has been changed later than this draft.', $args);
        $type = MessengerInterface::TYPE_ERROR;
      }
      else {
        $message = t('This @type is a draft of this <a href="@parent">@type</a>.', $args);
        $type = MessengerInterface::TYPE_STATUS;
      }
      \Drupal::messenger()->addMessage($message, $type);
      return $replicateParent;
    }
    return FALSE;
  }

  public static function createNewDraft(&$form, FormStateInterface $formState) {
    /** @var \Drupal\node\NodeForm $nodeForm */
    $nodeForm = $formState->getFormObject();
    /** @var \Drupal\node\NodeInterface $node */
    $node = $nodeForm->getEntity();
    if ($node->isPublished() && $formState->getValue('new_draft') === 'yes') {
      $node->setNewRevision(FALSE);
      /** @var \Drupal\replicate\Replicator $replicator */
      $replicator = \Drupal::service('replicate.replicator');
      /** @var \Drupal\node\NodeInterface $newNode */
      $newNode = $replicator->cloneEntity($node);
      $newNode->set('drafter_parent', $node->id());
      $newNode->set('path', [
        'pathauto' => PathautoState::SKIP,
        'pid' => NULL,
        'alias' => NULL,
      ]);
      $newNode->setUnpublished();
      $nodeForm->setEntity($newNode);
    }
  }

  public static function takeOver(&$form, FormStateInterface $formState) {
    /** @var \Drupal\node\NodeForm $nodeForm */
    $nodeForm = $formState->getFormObject();
    /** @var \Drupal\node\NodeInterface $node */
    $node = $nodeForm->getEntity();
    if ($node->isPublished()) {
      $form['#drafter_draft'] = $node;
      /** @var \Drupal\node\NodeInterface $parent */
      $parent = $node->drafter_parent->entity;
      self::copyUntranslatableFields($node, $parent);
      self::copyTranslatableFields($node, $parent);
      self::moveMenuLink($formState->getValue('menu'), $parent);
      self::fixJobItems($node, $parent);
      $parent->setNewRevision();
      $nodeForm->setEntity($parent);
    }
  }

  public static function deleteDraft(&$form, FormStateInterface $formState) {
    if (isset($form['#drafter_draft'])) {
      $form['#drafter_draft']->delete();
    }
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   * @param \Drupal\node\NodeInterface $parent
   */
  protected static function copyUntranslatableFields(NodeInterface $node, NodeInterface $parent) {
    $skip = $node->getTranslatableFields(FALSE);
    $skip['drafter_parent'] = TRUE;
    /** @var \Drupal\Core\Field\FieldItemListInterface $fieldItemList */
    foreach (array_diff_key($node->getFields(), $skip) as $fieldName => $fieldItemList) {
      $values = $fieldItemList->getValue();
      if ($fieldName === 'path') {
        $values[0]['source'] = '/node/' . $parent->id();
        // getValue() doesn't pick up computed properties.
        $values[0]['pathauto'] = $fieldItemList->first()->pathauto;
        // If the parent has an alias and changing it then updating that record
        // is correct as it will create the right redirect. If the parent has
        // no alias this is still correct because it'll be NULL causing a new
        // alias to be saved. The alias record on the draft in both cases will
        // be deleted along with the draft.
        $values[0]['pid'] = $parent->path->pid;
        $values[0]['langcode'] = LanguageInterface::LANGCODE_NOT_SPECIFIED;
        if (!$values[0]['alias']) {
          $values[0]['alias'] = $parent->path->alias;
        }
      }
      elseif ($fieldItemList->getFieldDefinition()->isReadOnly()) {
        continue;
      }
      $parent->set($fieldName, $values);
      // Bricks uses entity references instead of entity revision references to
      // refer paragraphs.
      if ($fieldItemList instanceof EntityReferenceFieldItemListInterface) {
        foreach ($fieldItemList->referencedEntities() as $paragraph) {
          if ($paragraph instanceof ParagraphInterface) {
            $paragraph->setParentEntity($parent, $fieldName);
          }
        }
        // When deleting this node, the paragraphs would be deleted too.
        $node->set($fieldName, NULL);
      }
    }
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   * @param \Drupal\node\NodeInterface $parent
   */
  protected static function copyTranslatableFields(NodeInterface $node, NodeInterface $parent) {
    foreach ($node->getTranslationLanguages() as $language) {
      $langcode = $language->getId();
      $nodeTranslation = $node->getTranslation($langcode);
      if (!$parent->hasTranslation($langcode)) {
        $parent->addTranslation($langcode);
      }
      $parentTranslation = $parent->getTranslation($langcode);
      foreach ($nodeTranslation->getTranslatableFields() as $fieldName => $fieldItemList) {
        if ($fieldItemList->getFieldDefinition()->isReadOnly()) {
          continue;
        }
        $parentTranslation->set($fieldName, $fieldItemList->getValue());
      }
    }
  }

  /**
   * @param $values
   * @param \Drupal\node\NodeInterface $parent
   */
  protected static function moveMenuLink($values, NodeInterface $parent) {
    if (!empty($values['entity_id'])) {
      $menuLink = MenuLinkContent::load($values['entity_id']);
      if ($menuLink) {
        $menuLink->link->uri = 'entity:node/' . $parent->id();
        $menuLink->save();
      }
    }
  }

  protected static function fixJobItems(NodeInterface $node, NodeInterface $parent) {
    $jobItemIds = \Drupal::entityQuery('tmgmt_job_item')
      ->condition('item_type', 'node')
      ->condition('item_id', (string) $node->id())
      ->execute();
    foreach (JobItem::loadMultiple($jobItemIds) as $jobItem) {
      $jobItem->item_id->value = $parent->id();
      $jobItem->save();
    }
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @return NodeInterface|FALSE
   */
  public static function getDraft(NodeInterface $node) {
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 0)
      ->condition('drafter_parent', $node->id())
      ->execute();
    return $nids ? Node::load(reset($nids)) : FALSE;
  }

}
